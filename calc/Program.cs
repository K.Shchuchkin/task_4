﻿using System;

namespace calc
{
    class Program
    {
        static void Main(string[] args)
        {
            Init();
        }
        #region Menu
        static void Init()
        {
            Console.WriteLine("Calculator initializing...");
            do
            {
                Console.WriteLine("Choose the operation:\n'm' - matrix calculator,\n'c' - regular calculator,\n'e' - exit,\n'h' - history");
                char.TryParse(Console.ReadLine().ToLower(), out char operation);
                switch (operation)
                {
                    case 'm':
                        Matrix();
                        break;
                    case 'c':
                        Calculator();
                        break;
                    case 'e':
                        return;
                    case 'h':
                        ShowHistory();
                        break;
                    default:
                        break;
                };
            } while (true);
        }
        #endregion
        #region Matrix initializing & filling
        static void MatrixInitializing(ref double[,] mtrx)
        {
            int hDimensionMtrx = default;
            int vDimensionMtrx = default;
            while (true)
            {
                Console.WriteLine("Enter matrix horizontal dimension:");
                while (!int.TryParse(Console.ReadLine(), out hDimensionMtrx) || hDimensionMtrx < 1)
                    Console.WriteLine("Sorry, matrix dimension should be greater than 0");
                Console.WriteLine("Enter matrix vertical dimension:");
                while (!int.TryParse(Console.ReadLine(), out vDimensionMtrx) || vDimensionMtrx < 1)
                    Console.WriteLine("Sorry, matrix dimension should be greater than 0");
                break;
            }
            mtrx = new double[vDimensionMtrx, hDimensionMtrx];
        }
        static void MatrixFill(ref double[,] mtrx)
        {
            for (int i = 0; i < mtrx.GetLength(0); i++)
                for (int j = 0; j < mtrx.GetLength(1); j++)
                {
                    Console.WriteLine($"Enter [{i + 1}, {j + 1}] matrix elemen):");
                    while(!double.TryParse(Console.ReadLine(), out mtrx[i, j]))
                        Console.WriteLine("Please, enter valid value");
                }
            for (int i = 0; i < mtrx.GetLength(0); i++)
            {
                Console.Write("|");
                for (int j = 0; j < mtrx.GetLength(1); j++)
                {
                    Console.Write(" {0} ", mtrx[i, j]);
                }
                Console.WriteLine("|");
            }
        }
        #endregion
        #region Matrix
        static void Matrix()
        {
            double[,] mtrx1 = null;
            double[,] mtrx2 = null;
            MatrixInitializing(ref mtrx1);
            MatrixInitializing(ref mtrx2);
            if (mtrx1.GetLength(1) == mtrx2.GetLength(1) && mtrx1.GetLength(0) == mtrx2.GetLength(0))
            {
                const string operations = "+-*ce";
                while (true)
                {
                    Console.WriteLine("Choose an operation:\n'+' - addition,\n'-' - substraction,\n'*' - multiplication,\n'e' - exit");
                    char.TryParse(Console.ReadLine().ToLower(), out char operation);
                    while (!operations.Contains(operation))
                        Console.WriteLine("Enter valid operation");
                    MatrixFill(ref mtrx1);
                    MatrixFill(ref mtrx2);
                    switch (operation)
                    {
                        case 'e':
                            return;
                        case '+':
                            mtrx1 = MatrixAddition(mtrx1, mtrx2);
                            break;
                        case '-':
                            mtrx1 = MatrixSubtraction(mtrx1, mtrx2);
                            break;
                        case '*':
                            mtrx1 = MatrixMultiplication(mtrx1, mtrx2);
                            break;
                        default:
                            break;
                    };
                    break;
                }
            }
            else if (mtrx1.GetLength(1) == mtrx2.GetLength(0))
            {
                const string operations = "*ce";
                while (true)
                {
                    Console.WriteLine("Only multiplication is available for this type of matrixes.");
                    Console.WriteLine("Choose an operation:\n'*' - multiplication,\n'c' - clear,\n'e' - exit");
                    char.TryParse(Console.ReadLine().ToLower(), out char operation);
                    while (!operations.Contains(operation))
                        Console.WriteLine("Enter valid operation");
                    MatrixFill(ref mtrx1);
                    MatrixFill(ref mtrx2);
                    switch (operation)
                    {
                        case '*':
                            mtrx1 = MatrixMultiplication(mtrx1, mtrx2);
                            break;
                        case 'e':
                            return;
                    }
                    break;
                }
            }
            else
            {
                Console.WriteLine("Sorry, you cannot perform any available operations for this type of matrix");
                return;
            }
        }
        #endregion
        #region MatrixOperations
        static double[,] MatrixMultiplication(double[,] Mtrx1, double[,] Mtrx2)
        {
            double[,] ResMtrx = new double[Mtrx1.GetLength(0), Mtrx2.GetLength(1)];
            for(int i = 0; i < ResMtrx.GetLength(0); i++)
                for(int j = 0; j < ResMtrx.GetLength(1); j++)
                    for(int c = 0; c < Mtrx2.GetLength(0); c++)
                        ResMtrx[i, j] += Mtrx1[i, c] * Mtrx2[c, j];
            Console.WriteLine("Result:");
            for (int i = 0; i < ResMtrx.GetLength(0); i++)
            {
                Console.Write("|");
                for (int j = 0; j < ResMtrx.GetLength(1); j++)
                {
                    Console.Write(" {0} ", ResMtrx[i, j]);
                }
                Console.WriteLine("|");
            }
            History($"{MatrixConversion(Mtrx1)} * \n{MatrixConversion(Mtrx2)} = \n{MatrixConversion(ResMtrx)}");
            return ResMtrx;    
        }
        static double[,] MatrixSubtraction(double[,] Mtrx1, double[,] Mtrx2)
        {
            double[,] ResMtrx = new double[Mtrx1.GetLength(0), Mtrx1.GetLength(1)];
            for (int i = 0; i < ResMtrx.GetLength(0); i++)
                for (int j = 0; j < ResMtrx.GetLength(1); j++)
                    ResMtrx[i, j] = Mtrx1[i, j] - Mtrx2[i, j];
            Console.WriteLine("Result:");
            for (int i = 0; i < ResMtrx.GetLength(0); i++)
            {
                Console.Write("|");
                for (int j = 0; j < ResMtrx.GetLength(1); j++)
                {
                    Console.Write(" {0} ", ResMtrx[i, j]);
                }
                Console.WriteLine("|");
            }
            History($"{MatrixConversion(Mtrx1)} - \n{MatrixConversion(Mtrx2)} = \n{MatrixConversion(ResMtrx)}");
            return ResMtrx;
        }
        static double[,] MatrixAddition(double[,] Mtrx1, double[,] Mtrx2)
        {
            double[,] ResMtrx = new double[Mtrx1.GetLength(0), Mtrx1.GetLength(1)];
            for (int i = 0; i < ResMtrx.GetLength(0); i++)
                for (int j = 0; j < ResMtrx.GetLength(1); j++)
                    ResMtrx[i, j] = Mtrx1[i, j] + Mtrx2[i, j];
            Console.WriteLine("Result:");
            for (int i = 0; i < ResMtrx.GetLength(0); i++)
            {
                Console.Write("|");
                for (int j = 0; j < ResMtrx.GetLength(1); j++)
                {
                    Console.Write(" {0} ", ResMtrx[i, j]);
                }
                Console.WriteLine("|");
            }
            History($"{MatrixConversion(Mtrx1)} + \n{MatrixConversion(Mtrx2)} = \n{MatrixConversion(ResMtrx)}");
            return ResMtrx;
        }
        #endregion
        #region RegularCalculator
        static void Calculator()
        {
            double a = default, b = default;
            const string operations = "+-*/e";
            Console.Write("Enter the first number: ");
            while (!double.TryParse(Console.ReadLine(), out a))
                Console.Write("Please, enter valid value: ");
            Console.Write("Enter the second number: ");
            while (!double.TryParse(Console.ReadLine(), out b))
                Console.Write("Please, enter valid value: ");
            Console.WriteLine("'+' - addition,\n'-' - substraction,\n'*' - multiplication,\n'/' - division,\n'e' - exit");
            char.TryParse(Console.ReadLine().ToLower(), out char operation);
            while (!operations.Contains(operation))
                Console.WriteLine("Enter valid operation");
            while (b == 0 && operation == '/')
            {
                Console.WriteLine("Sorry, you cannot divide by zero");
                return;
            }
            switch (operation)
            {
                case '+':
                    Console.WriteLine($"{a} + {b} = {a + b}");
                    History($"{a} + {b} = {a += b}");
                    break;
                case '-':
                    Console.WriteLine($"{a} - {b} = {a - b}");
                    History($"{a} - {b} = {a -= b}");
                    break;
                case '*':
                    Console.WriteLine($"{a} * {b} = {a * b}");
                    History($"{a} * {b} = {a *= b}");
                    break;
                case '/':
                    Console.WriteLine($"{a} / {b} = {Math.Round((a / b), 3)}");
                    History($"{a} / {b} = {Math.Round((a /= b), 3)}");
                    break;
                case 'e':
                    return;
                default:
                    break;
            };
        }
        #endregion
        #region History
        static string[] history = new string[20];
        static void ClearHistory()
        {
            for(int i = 0; i < history.Length; i++)
            {
                history[i] = string.Empty;
            }
        }
        static int note = 0;
        static void History(string NewLine)
        {
            if (note == 20 || note == 0)
            {
                ClearHistory();
                note = 0;
            }
            history[note++] += NewLine;
        }
        static void ShowHistory()
        {
            foreach (var line in history)
                if(line != string.Empty)
                    Console.WriteLine(line);
        }
        #endregion
        #region Formatting
        static string MatrixConversion(double[,] Mtrx)
        {
            string FormattedMatrix = default;
            for(int i = 0; i < Mtrx.GetLength(0); i++)
            {
                FormattedMatrix += "|";
                for(int j = 0; j < Mtrx.GetLength(1); j++)
                {
                    FormattedMatrix += (Mtrx[i, j] + " ");
                }
                FormattedMatrix += "|\n";
            }
            return FormattedMatrix;
        }
        #endregion
    }
}